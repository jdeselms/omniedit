# Omniedit

This is a simple POC of a "universal editor" which can view or edit any JSON object.

You can optionally define a schema, so that certain nodes of your object will be rendered by specific editors.

For example, you might have a string that represents a data, but in the schema, you could override the default string editor
and use a date picker instead.
