export interface SchemaComponentProps {
    value: any
    onChange?: (value: any) => void
}

export type SchemaComponent = (props: SchemaComponentProps) => JSX.Element
interface ObjectSchemaSpec {
    [field: string]: SchemaSpec
}

type ArraySchemaSpec = SchemaComponent[]

export type SchemaSpec = ObjectSchemaSpec | ArraySchemaSpec | SchemaComponent

