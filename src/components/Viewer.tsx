import { SchemaComponent, SchemaComponentProps } from "../types";
import * as Omni from "."
import { ArrayViewerImpl } from "./ArrayViewer";
import { ObjectViewerImpl } from "./ObjectViewer";
export interface ViewerProps extends SchemaComponentProps {
    schema?: SchemaComponent
}

export default function Viewer(props: ViewerProps) {
    if (props.schema) {
        return props.schema(props)
    } else {
        switch (typeof(props.value)) {
            case "string": return Omni.String(props)
            case "number": return Omni.Number(props)
            case "boolean": return Omni.Boolean(props)
            case "undefined": return Omni.Null(props)
            case "object": {
                if (props.value === null) return Omni.Null(props)
                if (Array.isArray(props.value)) return ArrayViewerImpl({ ...props, components: [Viewer]})
                return ObjectViewerImpl({ ...props, fields: {}})
            }
        }
    }

    throw new Error("can't figure out what kind of object this is")
}