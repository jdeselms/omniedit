import { SchemaComponent, SchemaComponentProps } from "../types";
import React from "react";
import Viewer from "./Viewer";

export interface ObjectComponentProps extends SchemaComponentProps {
    fields: Record<string, SchemaComponent>    
    includeExtraItems?: boolean
    keyValueViewer?: (props: KeyValueViewerProps) => JSX.Element
    wrapperViewer?: (props: ObjectWrapperViewerProps) => JSX.Element
}

export interface ObjectViewerOptions {
    /**
     * if false, only fields explicitly defined in the schema will be rendered.
     * if true, additional fields will be rendered using the default viewer.
     */
    includeExtraItems?: boolean

    /**
     * If specified, this will emit the key value pair.
     */
    keyValueViewer?: (props: KeyValueViewerProps) => JSX.Element
}

export default function ObjectViewer(fields: Record<string, SchemaComponent>, opts: ObjectViewerOptions = {}): SchemaComponent {
    return (props: SchemaComponentProps) => ObjectViewerImpl({
        ...props,
        fields,
        includeExtraItems: !!opts.includeExtraItems
    })
}

export function ObjectViewerImpl(props: ObjectComponentProps) {
    const entries = props.value as {};
    const elements: JSX.Element[] = [];

    const keyValueViewer = props.keyValueViewer || KeyValueViewer
    const wrapperViewer = props.wrapperViewer || ObjectWrapperViewer

    function getNewValue(field: string, newValue: any): {} {
        return { ...entries, [field]: newValue };
    }

    const writtenFields = new Set<string>()

    for (const [key, component] of Object.entries(props.fields)) {
        const onChange = props.onChange ? ((val: any) => props.onChange!(getNewValue(key, val))) : undefined;
        const value = props.value[key]

        const el = component({ value, onChange })
        if (el !== undefined) {
            elements.push(keyValueViewer({ key, value: el }))
            writtenFields.add(key)
        }
    }

    if (props.includeExtraItems) {
        for (const [key, value] of Object.entries(props.value)) {
            if (!writtenFields.has(key)) {
                const onChange = props.onChange ? ((val: any) => props.onChange!(getNewValue(key, val))) : undefined;
                const el = Viewer({ value, onChange })
                elements.push(keyValueViewer({ key, value: el }));
            }
        }
    }

    return wrapperViewer({ children: elements })
}

export interface KeyValueViewerProps {
    key: string
    value: JSX.Element
}

export function KeyValueViewer(props: KeyValueViewerProps): JSX.Element {
    return <tr key={props.key}><td><b>{props.key}</b></td><td>{props.value}</td></tr>
}

export interface ObjectWrapperViewerProps {
    children: JSX.Element[]
}

export function ObjectWrapperViewer(props: ObjectWrapperViewerProps): JSX.Element {
    return <table><tbody>{props.children}</tbody></table>
}