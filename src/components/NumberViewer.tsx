import { SchemaComponentProps } from "../types";
import React from "react";

export default function NumberViewer(props: SchemaComponentProps) {
    if (props.onChange) {
        return <input type="text" value={props.value} onChange={evt => props.onChange?.(evt.target.value)}/>
    } else {
        return <>{props.value}</>;
    }
}
