import { SchemaComponentProps } from "../types";
import React from "react";

export default function NullViewer(props: SchemaComponentProps) {
    if (props.value === undefined) {
        return <i>undefined</i>;
    } else {
        return <i>null</i>;
    }
}
