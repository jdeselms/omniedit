import { SchemaComponentProps, SchemaComponent } from "../types";
import React from "react";

export interface ArrayComponentProps extends SchemaComponentProps {
    components: Array<SchemaComponent>
}

export default function ArrayViewer(components: SchemaComponent[] | SchemaComponent): SchemaComponent {

    const componentArray = Array.isArray(components)
        ? components
        : [ components ];

    return (props: SchemaComponentProps) => ArrayViewerImpl({
        ...props,
        components: componentArray
    })
}


export function ArrayViewerImpl(props: ArrayComponentProps) {
    const entries = props.value as [];
    const elements: JSX.Element[] = [];

    function getNewValue(index: number, newValue: any): {} {
        const arr: any[] = [...entries];
        arr[index] = newValue;
        return arr;
    }

    let i = 0;
    let index = 0;

    for (const entry of entries) {
        const idx = index;
        const onChange = props.onChange ? ((val: any) => props.onChange!(getNewValue(idx, val))) : undefined;
        const el = props.components[i]({ value: entry, onChange })
        if (el !== undefined) {
            elements.push(<li key={index}>{el}</li>);

            i = i + 1 % props.components.length;
        }
        index++;
    }
    return <ul>{elements}</ul>;
}
