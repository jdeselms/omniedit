import { SchemaComponentProps } from "../types";
import React from "react";

export default function BooleanViewer(props: SchemaComponentProps) {
    if (props.onChange) {
        return <input type="checkbox" checked={props.value} onChange={evt => props.onChange?.(!props.value)}/>
    } else {
        return <>{props.value ? "true" : "false"}</>;
    }
}
