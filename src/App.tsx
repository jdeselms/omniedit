import React from 'react';
import './App.css';
import Viewer from './components/Viewer';
import { SchemaSpec } from './types';

import * as Omni from "./components"

function App() {
  // Define the object that we want to view/edit
  const [ model, updateModel ] = React.useState({
    name: "Jim",
    address: {
      street1: "16 Henshaw St",
      street2: "Suite 1",
      city: "West Newton",
      state: "MA",
      zipCode: 12345
    },
    friends: [ "John", "Susan" ],
    children: [ "Micah", "Penelope" ],
    hasFriends: true,
    dingleberry: "Pie"
  })

  // Define the schema. This is optional; if you don't supply a schema, it'll use the default JSON editor/viewer.
  // In this case, we'll override the viewer for the zipCode field.
  const schema: SchemaSpec = Omni.Object({
    name: Omni.String,
    address: Omni.Object({
      street1: Omni.String,
      street2: Omni.String,
      city: Omni.String,
      state: Omni.String,
      zipCode: Omni.Number
    }),
    friends: Omni.Array(Omni.String),
    children: Omni.Array([Omni.String]),
    hasFriends: Omni.Boolean
  }, { includeExtraItems: true})

  // Show two versions of the viewer. The first one is editable because we pass it an "onChange" handler.
  // The second one just views the object in read-only mode.
  return (<>
    <Viewer value={model} schema={schema} onChange={updateModel}/>
    <hr/>
    <Viewer value={model} schema={schema}/>
  </>);
}
 
export default App;
